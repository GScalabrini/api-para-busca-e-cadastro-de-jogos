# GameInfoSearch API

Bem-vindo à API GameInfoSearch! Esta API permite cadastrar novos games e consultar informações sobre eles, incluindo detalhes sobre os personagens associados a cada jogo.

## Visão Geral

A API GameInfoSearch oferece os seguintes recursos:

- Cadastro de novos games com informações como nome, gênero e detalhes.
- Consulta de informações detalhadas sobre um jogo específico, incluindo os personagens associados a ele.

## Tecnologias Utilizadas

- Java
- Spring Boot
- Spring Web
- Spring DevTools
- Spring Data JPA
- JSON

## Como Usar

### Cadastro de Novos Games

Para cadastrar um novo jogo, envie uma solicitação HTTP `POST` para o endpoint `/game/adicionar`, fornecendo as seguintes informações no corpo da solicitação em formato JSON:

```json
{
    "nome": "Nome do Jogo",
    "genero": "Gênero do Jogo",
    "detalhes": "Detalhes sobre o Jogo"
}
```

### Exemplo: 

POST /game/adicionar
Content-Type: application/json

```json
{
    "nome": "The Legend of Zelda: Breath of the Wild",
    "genero": "Aventura",
    "detalhes": "Jogo de aventura e exploração com elementos de RPG. Lançado para Nintendo Switch."
}
```

## Consulta de Informações de um Jogo

Para consultar informações detalhadas sobre um jogo específico, envie uma solicitação HTTP GET para o endpoint /game/consulta/{id}, onde {id} é o identificador único do jogo desejado.

### Exemplo de solicitação:

GET /game/consulta/1

## Documentação da API
Para mais detalhes sobre como usar a API, consulte a documentação completa em link para a documentação. (Em breve)

## Contribuindo
Se você deseja contribuir para o desenvolvimento da API GameInfoSearch, sinta-se à vontade para abrir um pull request ou relatar problemas na página de issues do projeto.