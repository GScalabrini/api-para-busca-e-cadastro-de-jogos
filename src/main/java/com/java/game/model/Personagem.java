package com.java.game.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class Personagem {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String nome;
	
	private String detalhes;
	
	@ManyToOne
    @JoinColumn(name = "game")
    private Game game;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Game getGame() {
		return game;
	}
	
	public void setGame(Game game) {
		this.game = game;
	}
	
	public String getDetalhes() {
		return detalhes;
	}
	
	public void setDetalhes(String detalhes) {
		this.detalhes = detalhes;
	}
	
	public int hashcode(){
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id==null) ? 0:id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this==obj) {return true;}
		if (obj==null) {return false;}
		Personagem other = (Personagem) obj;
		if (id==null) {
			if(other.id != null) {
				return false;
			} else if(!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}
}
