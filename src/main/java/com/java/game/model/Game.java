package com.java.game.model;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class Game {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false) 
	private String nome;
	
	@Column(nullable = false) 
	private String genero;
	
	@OneToMany(mappedBy = "game")
    private List<Personagem> personagens;
	
	public Game( ) {
		
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getGenero() {
		return genero;
	}
	
	public void setGenero(String genero) {
		this.genero = genero;
	}
	
	public int hashcode(){
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id==null) ? 0:id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this==obj) {return true;}
		if (obj==null) {return false;}
		Game other = (Game) obj;
		if (id==null) {
			if(other.id != null) {
				return false;
			} else if(!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}
}
