package com.java.game.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.java.game.model.Game;
import com.java.game.model.Personagem;

import com.java.game.repository.GameRepository;
import com.java.game.repository.PersonagemRepository;

import com.java.game.dto.GameComPersonagensDTO;

@RestController
@RequestMapping("/game")
public class GameController {
	
	@Autowired
	private GameRepository gameRepository;
	@Autowired
	private PersonagemRepository personagemRepository;
	
	@GetMapping("/listar")
	public List <Game> listar(){
		return gameRepository.findAll();
	}
	
	//NOTE Lembrar de estudar esse metodo
	@GetMapping("/consulta/{id}")
	public ResponseEntity<GameComPersonagensDTO> consultaPorGame(@PathVariable Long id) {
	    Optional<Game> gameOptional = gameRepository.findById(id);
	    
	    if (gameOptional.isPresent()) {
	        Game game = gameOptional.get();
	        
	        List<Personagem> personagens = personagemRepository.findByGame(game);
	        
	        GameComPersonagensDTO responseDTO = new GameComPersonagensDTO();
	        responseDTO.setGame(game);
	        responseDTO.setPersonagens(personagens);
	        
	        return ResponseEntity.ok().body(responseDTO);
	    } else {
	        return ResponseEntity.notFound().build();
	    }
	}

	@PostMapping("/adicionar")
	public Game adicionar(@RequestBody Game game) {
	    return gameRepository.save(game);
	}
}