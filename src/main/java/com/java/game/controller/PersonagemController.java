package com.java.game.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.java.game.model.Personagem;
import com.java.game.model.Game;

import com.java.game.repository.PersonagemRepository;
import com.java.game.repository.GameRepository;

import com.java.game.request.PersonagemRequest;;

@RestController
@RequestMapping("/personagem")
public class PersonagemController {
	
	@Autowired
	private PersonagemRepository personagemRepository;
	
	@Autowired
	private GameRepository gameRepository;
	
	@GetMapping("/listar")
	public List <Personagem> listar(){
		return personagemRepository.findAll();
	}
	
	@GetMapping("/consulta/{id}")
	public ResponseEntity<Personagem> consultaPorPersonagem(@PathVariable Long id) {
        return personagemRepository.findById(id)
                .map(personagem -> ResponseEntity.ok().body(personagem))
                .orElse(ResponseEntity.notFound().build());
    }
	
	@PostMapping("/adicionar")
	public Personagem adicionar(@RequestBody PersonagemRequest request) {
	    Optional<Game> gameOptional = gameRepository.findById(request.getGameId());
	    
	    if(gameOptional.isPresent()) {
	        Personagem personagem = request.getPersonagem();
	        personagem.setGame(gameOptional.get());
	        return personagemRepository.save(personagem);
	    } else {
	        throw new RuntimeException("Jogo não encontrado com o ID: " + request.getGameId());
	    }
	}
	
}