package com.java.game.dto;

import java.util.List;

import com.java.game.model.Game;
import com.java.game.model.Personagem;

public class GameComPersonagensDTO {
    private Game game;
    private List<Personagem> personagens;
	public Game getGame() {
		return game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
	public List<Personagem> getPersonagens() {
		return personagens;
	}
	public void setPersonagens(List<Personagem> personagens) {
		this.personagens = personagens;
	}

}