package com.java.game.request;

import com.java.game.model.Personagem;

public class PersonagemRequest {
    private Long gameId;
    private Personagem personagem;
	public Long getGameId() {
		return gameId;
	}
	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}
	public Personagem getPersonagem() {
		return personagem;
	}
	public void setPersonagem(Personagem personagem) {
		this.personagem = personagem;
	}

}