package com.java.game.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.java.game.model.Game;
import com.java.game.model.Personagem;

@Repository
public interface PersonagemRepository extends JpaRepository<Personagem, Long> {

	List<Personagem> findByGame(Game game);

}