package com.java.game;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GameInfoSearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(GameInfoSearchApplication.class, args);
	}

}
